# import libraries
import urllib3
from bs4 import BeautifulSoup
import time
import certifi
import datetime



#####################################
# TO DO
# a) Add non-european exonyms
# 1) Add caching to this app (i.e. once per day, etc)
# 2) Be selective on cells to stop weird links coming through (i.e. LH col could be id'd as len <=25
#####################################

# Output results to a file
def output_file(mypath, mylist):
    # using list comprehension to remove blanks
    mylistclean = [i for i in mylist if i]
    with open(mypath, 'w',encoding='utf8') as f:
        # this is a list of lists so iterate through each row
        for item in mylistclean:
            for element in item:
                f.write(str(element) + ',')
            f.write('\n')
    print('File written: ' + mypath)

# declare list for output
exonyms = list()

# European exonymns
def exonyms_europe(exonyms):
    # open the url
    http = urllib3.PoolManager(
        cert_reqs='CERT_REQUIRED',
        ca_certs=certifi.where())
    url = []
    url.append('https://en.wikipedia.org/wiki/Names_of_European_cities_in_different_languages_(A)')
    url.append('https://en.wikipedia.org/wiki/Names_of_European_cities_in_different_languages:_B')
    url.append('https://en.wikipedia.org/wiki/Names_of_European_cities_in_different_languages:_C%E2%80%93D#C')
    url.append('https://en.wikipedia.org/wiki/Names_of_European_cities_in_different_languages:_E%E2%80%93H')
    url.append('https://en.wikipedia.org/wiki/Names_of_European_cities_in_different_languages:_I%E2%80%93L')
    url.append('https://en.wikipedia.org/wiki/Names_of_European_cities_in_different_languages:_M%E2%80%93P')
    url.append('https://en.wikipedia.org/wiki/Names_of_European_cities_in_different_languages:_Q%E2%80%93T')
    url.append('https://en.wikipedia.org/wiki/Names_of_European_cities_in_different_languages:_U%E2%80%93Z')

    for page in url:
        response = http.request('GET', page)

        # parse the html using beautiful soup and store in variable `soup`
        soup = BeautifulSoup(response.data, features="lxml")

        #table = soup.find('table',attrs={'class':'wikitable'})
        for table in soup.findChildren('table',attrs={'class':'wikitable'}):
            rows = table.findChildren(['tr'])

            for row in rows:
                cells = row.findChildren('td')
                bTags = list()
                for cell in cells:
                    if len(cell.text) <= 30:
                        for i in cell.findAll('a'):
                            if len(i.text) > 1:
                                bTags.append(i.text)
                    else:
                        for i in cell.findAll('b'):
                            if len(i.text) > 1:
                                bTags.append(i.text)

                #Dedupe and print list
                bTags=list(dict.fromkeys(bTags))
                #print('completed url:', page)
                exonyms.append(bTags)
                print(bTags)
        # wait for 2 seconds
        time.sleep(1)

# European exonymns
def exonyms_other(exonyms):
    # open the url
    http = urllib3.PoolManager(
        cert_reqs='CERT_REQUIRED',
        ca_certs=certifi.where())
    url = []
    url.append('https://en.wikipedia.org/wiki/List_of_countries_and_dependencies_and_their_capitals_in_native_languages')

    for page in url:
        response = http.request('GET', page)

        # parse the html using beautiful soup and store in variable `soup`
        soup = BeautifulSoup(response.data, features="lxml")

        #table = soup.find('table',attrs={'class':'wikitable'})
        for table in soup.findChildren('table',attrs={'class':'wikitable'}):
            rows = table.findChildren(['tr'])

            for row in rows:
                cells = row.findChildren('td')
                bTags = list()

                c1 = 0


                for cell in cells:
                    c1 = c1 + 1
                    if c1 == 999:  # if set to one will extract country names
                        for i in cell.findAll('a'):
                            if len(i.text) > 1:
                                bTags.append(i.text)
                                break
                    elif c1 == 2:
                        for i in cell.findAll('a'):
                            if len(i.text) > 1:
                                bTags.append(i.text)
                                break
                    elif c1 == 4:
                        for i in cell.findAll('b'):
                            if len(i.text) > 1:
                                bTags.append(i.text)

                #Dedupe and print list
                bTags=list(dict.fromkeys(bTags))
                #print('completed url:', page)
                exonyms.append(bTags)
               # print(bTags)
        # wait for 2 seconds
        time.sleep(1)


    #merge_lists(exonyms)

def merge_lists(mylist):

    clean_values = list()

    for i in mylist:
        main_token = i[0].upper() #set root token
        del i[0] #delete the root
        for j in i:
            new_row = main_token + '|' + j.upper()
            clean_values.append(new_row)
    clean_values.sort()
    for i in clean_values:
        print(i)


# get european exonymns
exonyms_europe(exonyms)
exonyms_other(exonyms)

# write file output
filename = datetime.datetime.now().strftime('european_synonyms_%H_%M_%S_%d_%m_%Y.txt')
output_file('output/' + filename, exonyms)

